package com.example.ebasimcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EbasimCicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(EbasimCicdApplication.class, args);
        System.out.println("Hello");
    }

}
